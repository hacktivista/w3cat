#!/usr/bin/env node

import { firefox } from 'playwright';
import { JSDOM } from 'jsdom';
import{ Readability, isProbablyReaderable } from '@mozilla/readability';
import cliHtml from 'cli-html';

async function downloadAndParseWebsite(url) {
  const browser = await firefox.launch();
  const context = await browser.newContext();
  const page = await context.newPage();

  await page.goto(url, { waitUntil: 'domcontentloaded' });

  const html = await page.content();
  const dom = new JSDOM(html, { url });

  if (isProbablyReaderable(dom.window.document)) {
    const reader = new Readability(dom.window.document);
    const article = reader.parse();
    console.log(cliHtml(article.content));
  } else {
    console.log(cliHtml(dom.window.document.body.innerHTML));
  }

  await browser.close();
}

if (process.argv.length < 3) {
  console.log(`Usage: w3cat <url>`);
  process.exit(1);
}

downloadAndParseWebsite(process.argv[2]);
